package ru.t1.ytarasov.tm;

import ru.t1.ytarasov.tm.component.Bootstrap;

public class Application {

    public static void main(String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.runArguments(args);
    }

}
