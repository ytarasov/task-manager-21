package ru.t1.ytarasov.tm.command.project;

import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.ytarasov.tm.model.Project;
import ru.t1.ytarasov.tm.util.TerminalUtil;

public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    public static final String NAME = "project-show-by-id";

    public static final String DESCRIPTION = "Find project by id";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final String userId = getAuthService().getUserId();
        final Project project = getProjectService().findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
