package ru.t1.ytarasov.tm.exception.field;

public final class IdEmptyException extends AbstractFieldException {

    public IdEmptyException() {
        super("Error! ID is empty...");
    }

}
