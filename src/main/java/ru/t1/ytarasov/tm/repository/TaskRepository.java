package ru.t1.ytarasov.tm.repository;

import ru.t1.ytarasov.tm.api.repository.ITaskRepository;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.model.Task;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Override
    public Task create(String userId, String name) {
        Task task = new Task(name);
        task.setUserId(userId);
        return add(task);
    }

    @Override
    public Task create(String userId, String name, String description) {
        Task task = new Task(name, description);
        task.setUserId(userId);
        return add(task);
    }

    @Override
    public Task create(String userId, String name, Status status) {
        Task task = new Task(name,status);
        task.setUserId(userId);
        return add(task);
    }

    @Override
    public Task create(String userId, String name, String description, Status status) {
        Task task = new Task(name,description, status);
        task.setUserId(userId);
        return add(task);
    }

    @Override
    public List<Task> findAllTasksByProjectId(String userId, final String projectId) {
        return models
                .stream()
                .filter(m -> projectId.equals(m.getProjectId()))
                .filter(m -> userId.equals(m.getUserId()))
                .collect(Collectors.toList());
    }

}
