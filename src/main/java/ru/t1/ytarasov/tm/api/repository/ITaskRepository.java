package ru.t1.ytarasov.tm.api.repository;

import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    Task create(String userId, String name);

    Task create(String userId, String name, String description);

    Task create(String userId, String name, Status status);

    Task create(String userId, String name, String description, Status status);

    List<Task> findAllTasksByProjectId(String userId, String projectId);

}
